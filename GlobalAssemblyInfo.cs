﻿using System.Reflection;

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Patch Number
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyCompany("VietHoang")]
[assembly: AssemblyCopyright("Copyright © VietHoang 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("0.0.1")]
[assembly: AssemblyInformationalVersion("0.0.1")]
[assembly: AssemblyFileVersion("0.0.1")]