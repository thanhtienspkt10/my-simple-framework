using AutoMapper;
using VietHoang.Tekcent.Domain.Models.Page;
using VietHoang.Tekcent.Web.Models;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(VietHoang.Tekcent.Web.App_Start.AutoMapperConfig), "Start")]
namespace VietHoang.Tekcent.Web.App_Start
{
    public static class AutoMapperConfig
    {
        public static void Start()
        {
            RegisterAutoMapper();
        }

        public static void RegisterAutoMapper()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<IPage, PageViewModel>());
        }
    }
}