﻿using System.Runtime.CompilerServices;
using Glass.Mapper.Sc.IoC;
using Microsoft.Extensions.DependencyInjection;
using VietHoang.Ioc;
using VietHoang.Tekcent.Services;
using VietHoang.Tekcent.Services.Interfaces;
using Sitecore.DependencyInjection;

namespace VietHoang.Tekcent.Web.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        [MethodImpl(MethodImplOptions.NoInlining)]
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IMapModelService, MapModelService>();
            serviceCollection.AddTransient<IApplicationService, ApplicationService>();
            serviceCollection.AddTransient<ISitecoreContextFactory, Services.Factories.SitecoreContextFactory>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}
