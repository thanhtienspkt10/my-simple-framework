﻿using Glass.Mapper.Sc.Fields;
using VietHoang.Tekcent.Domain.Models.Page;

namespace VietHoang.Tekcent.Web.Models
{
    public class PageViewModel : StandardItemViewModel, IPage
    {
        public string Headline { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public string PageTitle { get; set; }
        public string PageDescription { get; set; }
        public string PageKeywords { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public Image MetaImage { get; set; }
        public string MetaType { get; set; }
        public string MetaCard { get; set; }
        public ISiteRoot SiteRoot { get; set; }
		
		
    }
}