using System;
using VietHoang.Tekcent.Domain.Models.BaseModels;
using Sitecore.Data;
using Sitecore.Globalization;

namespace VietHoang.Tekcent.Web.Models
{
    public class StandardItemViewModel : IStandardItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string SortOrder { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Language Language { get; set; }
        public ItemUri Uri { get; set; }
        public string DisplayName { get; set; }
        public int Version { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public string FullPath { get; set; }
        public string FullUrl { get; set; }
    }
}