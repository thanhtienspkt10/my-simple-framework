﻿using System.Web.Mvc;
using VietHoang.Tekcent.Services.Interfaces;
using VietHoang.Tekcent.Web.Utilities;
using SC = Sitecore;

namespace VietHoang.Tekcent.Web.Controllers
{
    public class BaseController : Controller
    {
        public BaseController(IMapModelService mapModelService, IApplicationService applicationService)
        {
            MapModelService = mapModelService;
            ApplicationService = applicationService;
        }

        //[Dependency]
        public IMapModelService MapModelService { get; set; }

        //[Dependency]
        public IApplicationService ApplicationService { get; set; }

        public string DefaultViewName
        {
            get
            {
                var actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                return GetViewName(SC.Context.Site.Name, actionName);
            }
        }

        protected string GetViewName(string siteName, string actionName)
        {
            var controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            return $"~/Views/{siteName}/{controllerName}/{actionName}.cshtml";
        }

        protected string GetViewName(string siteName, string controllerName, string actionName)
        {
            return WebUtility.GetMvcViewName(siteName, controllerName, actionName);
        }
    }
}