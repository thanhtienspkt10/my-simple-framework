﻿using System.Web.Mvc;
using VietHoang.Tekcent.Services.Interfaces;
using VietHoang.Tekcent.Web.Models;

namespace VietHoang.Tekcent.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IMapModelService mapModelService, IApplicationService applicationService)
            : base(mapModelService, applicationService)
        {
        }

        public ActionResult Index()
        {
            var homepage = ApplicationService.GetHomePage();
            var model = MapModelService.Map<PageViewModel>(homepage);
            return View(this.DefaultViewName, model);
        }
    }
}