﻿namespace VietHoang.Tekcent.Web.Utilities
{
    public static class WebUtility
    {
        public static string GetMvcViewName(string siteName, string controllerName, string actionName)
        {
            return $"~/Views/{siteName}/{controllerName}/{actionName}.cshtml";
        }
    }
}