﻿namespace VietHoang.Tekcent.Domain.Constants
{
    public static class FieldIds
    {
        public struct SiteRoot
        {
            public const string Logo = "{B5E7F5E1-87C1-40BC-973A-561177A7DF6A}";
            public const string Copyright = "{007842A1-5006-419F-BAD1-41FC186F8778}";
            public const string OrganisationName = "{F7A13916-9F08-44A0-BFAC-A65B999EE0C4}";
            public const string OrganisationAddress = "{92E04668-E00B-42D5-B3B0-ADC7FCDB720F}";
            public const string OrganisationPhone = "{8FF7BDD1-2474-45BE-BADD-DA86D59030D9}";
            public const string OrganisationEmail = "{A9E8449B-44AF-44A5-BF99-9D79B041A91E}";
        }

        public struct Metadata
        {
            public const string PageTitle = "{56231F54-2712-4D6C-93B6-BEA8EF6F14E6}";
            public const string PageDescription = "{490DD3DE-3FEC-4543-9EC2-D8425E38D2AA}";
            public const string PageKeywords = "{E2EA6DFA-85EB-40EF-93D1-8AAE1E465590}";
            public const string MetaTitle = "{EF79008B-82A1-4C4A-8020-AF231407AF0E}";
            public const string MetaDescription = "{CDCF5F10-49EC-4CA2-987A-449DEA38E8B5}";
            public const string MetaImage = "{AE893AF1-1D53-42EF-A56E-443E520498E9}";
            public const string MetaType = "{41C7D91E-A871-438F-AC15-E3478EEE840D}";
            public const string MetaCard = "{427E395F-B1C7-4CE8-89D8-CB0806B7E41C}";
        }

        public struct BaseContent
        {
            public const string Headline = "{A1B4BC3F-CFF9-4EBD-8C43-310119A13D34}";
            public const string ShortDescription = "{08F17B4D-2D1F-4F0A-82B9-886621612D17}";
            public const string Body = "{D4C849A0-F09F-409A-850B-8E72A560DCD3}";
        }
    }
}
