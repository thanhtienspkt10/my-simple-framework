﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using VietHoang.Tekcent.Domain.Constants;
using VietHoang.Tekcent.Domain.Models.BaseModels;
using VietHoang.Tekcent.Domain.Constants;
using VietHoang.Tekcent.Domain.Models.BaseModels;

namespace VietHoang.Tekcent.Domain.Models.Page
{
    public interface IMetadata : IStandardItem
    {
        [SitecoreField(FieldIds.Metadata.PageTitle)]
        string PageTitle { get; set; }
        [SitecoreField(FieldIds.Metadata.PageDescription)]
        string PageDescription { get; set; }
        [SitecoreField(FieldIds.Metadata.PageKeywords)]
        string PageKeywords { get; set; }
        [SitecoreField(FieldIds.Metadata.MetaTitle)]
        string MetaTitle { get; set; }
        [SitecoreField(FieldIds.Metadata.MetaDescription)]
        string MetaDescription { get; set; }
        [SitecoreField(FieldIds.Metadata.MetaImage)]
        Image MetaImage { get; set; }
        [SitecoreField(FieldIds.Metadata.MetaType)]
        string MetaType { get; set; }
        [SitecoreField(FieldIds.Metadata.MetaCard)]
        string MetaCard { get; set; }
    }
}
