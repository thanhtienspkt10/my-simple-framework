﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Tekcent.Domain.Constants;

namespace VietHoang.Tekcent.Domain.Models.Page
{
    public interface IPage : IBaseContent, IMetadata
    {
        [SitecoreQuery("./ancestor::*[@@templateid='" + TemplateIds.SiteRoot + "']", IsRelative = true)]
        ISiteRoot SiteRoot { get; set; }
    }
}
