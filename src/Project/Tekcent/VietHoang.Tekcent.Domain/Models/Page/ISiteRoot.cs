﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using VietHoang.Tekcent.Domain.Constants;
using VietHoang.Tekcent.Domain.Models.BaseModels;

namespace VietHoang.Tekcent.Domain.Models.Page
{
    public interface ISiteRoot : IStandardItem
    {
        [SitecoreField(FieldIds.SiteRoot.Logo)]
        Image Logo { get; set; }
        [SitecoreField(FieldIds.SiteRoot.Copyright)]
        string Copyright { get; set; }
        [SitecoreField(FieldIds.SiteRoot.OrganisationName)]
        string OrganisationName { get; set; }
        [SitecoreField(FieldIds.SiteRoot.OrganisationAddress)]
        string OrganisationAddress { get; set; }
        [SitecoreField(FieldIds.SiteRoot.OrganisationPhone)]
        string OrganisationPhone { get; set; }
        [SitecoreField(FieldIds.SiteRoot.OrganisationEmail)]
        string OrganisationEmail { get; set; }
    }
}
