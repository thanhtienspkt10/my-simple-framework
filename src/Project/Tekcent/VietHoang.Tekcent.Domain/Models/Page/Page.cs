﻿using System;
using Glass.Mapper.Sc.Fields;
using VietHoang.Tekcent.Domain.Models.BaseModels;
using Sitecore.Data;
using Sitecore.Globalization;

namespace VietHoang.Tekcent.Domain.Models.Page
{
    public class Page : IPage
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public string FullPath { get; set; }
        public string FullUrl { get; set; }
        public string Name { get; set; }
        public Guid TemplateId { get; set; }
        public string TemplateName { get; set; }
        string IStandardItem.SortOrder { get; set; }
        public int SortOrder { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Language Language { get; set; }
        public ItemUri Uri { get; set; }
        public string DisplayName { get; set; }
        public string Headline { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public string PageTitle { get; set; }
        public string PageDescription { get; set; }
        public string PageKeywords { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public Image MetaImage { get; set; }
        public string MetaType { get; set; }
        public string MetaCard { get; set; }
        public ISiteRoot SiteRoot { get; set; }
    }
}
