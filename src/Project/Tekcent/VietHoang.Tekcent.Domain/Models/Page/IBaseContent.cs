﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Tekcent.Domain.Constants;
using VietHoang.Tekcent.Domain.Models.BaseModels;
using VietHoang.Tekcent.Domain.Constants;
using VietHoang.Tekcent.Domain.Models.BaseModels;

namespace VietHoang.Tekcent.Domain.Models.Page
{
    public interface IBaseContent : IStandardItem
    {
        [SitecoreField(FieldIds.BaseContent.Headline)]
        string Headline { get; set; }
        [SitecoreField(FieldIds.BaseContent.ShortDescription)]
        string ShortDescription { get; set; }
        [SitecoreField(FieldIds.BaseContent.Body)]
        string Body { get; set; }
    }
}
