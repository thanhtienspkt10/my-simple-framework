﻿using VietHoang.Tekcent.Domain.Models.Page;

namespace VietHoang.Tekcent.Services.Interfaces
{
    public interface IApplicationService
    {
        IPage GetHomePage();
    }
}
