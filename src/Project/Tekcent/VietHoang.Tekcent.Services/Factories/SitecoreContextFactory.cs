﻿using Glass.Mapper;
using Glass.Mapper.Sc;
using Glass.Mapper.Sc.IoC;

namespace VietHoang.Tekcent.Services.Factories
{
    public class SitecoreContextFactory : ISitecoreContextFactory
    {
        public ISitecoreContext GetSitecoreContext()
        {
            return new SitecoreContext();
        }

        public ISitecoreContext GetSitecoreContext(string contextName)
        {
            return new SitecoreContext(contextName);
        }

        public ISitecoreContext GetSitecoreContext(Context context)
        {
            return new SitecoreContext(context);
        }
    }
}
