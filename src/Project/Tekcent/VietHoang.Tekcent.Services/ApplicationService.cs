using Glass.Mapper.Sc.IoC;
using VietHoang.Tekcent.Domain.Models.Page;
using VietHoang.Tekcent.Services.Abstractions;
using VietHoang.Tekcent.Services.Interfaces;
using SC = Sitecore;

namespace VietHoang.Tekcent.Services
{
    public class ApplicationService : BaseService, IApplicationService
    {
        public ApplicationService(ISitecoreContextFactory scContextFactory)
            : base(scContextFactory)
        {
        }

        public IPage GetHomePage()
        {
            return ScContext.GetItem<IPage>($"/sitecore/content/{SC.Context.Site.Name}/home",
                isLazy: false, inferType: false);
        }
    }
}
