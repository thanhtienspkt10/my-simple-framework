using Glass.Mapper.Sc;
using Glass.Mapper.Sc.IoC;

namespace VietHoang.Tekcent.Services.Abstractions
{
    public abstract class BaseService
    {
        private readonly ISitecoreContextFactory _scContextFactory;

        protected BaseService(ISitecoreContextFactory scContextFactory)
        {
            _scContextFactory = scContextFactory;
        }

        protected ISitecoreContext ScContext
        {
            get { return _scContextFactory.GetSitecoreContext(); }
        }

        protected ISitecoreService ScService { get; set; }
    }
}
