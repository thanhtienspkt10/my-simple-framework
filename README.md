For more information, please visit the blog https://buoctrenmay.wordpress.com/2017/02/06/a-simple-framework-for-sitecore-xp-8-development/

## Main Technologies:
- .Net Framework version 4.6
- Sitecore XP version 8.2 or later
- Glass Mapper Sitecore version 4.3.4.196: an object mapping framework for Sitecore https://github.com/mikeedwards83/Glass.Mapper
- Unicorn version 3.3.2: a Sitecore utility designed to simplify deployment of Sitecore items across environments automatically https://github.com/kamsar/Unicorn
- Microsoft Visual Studio Unit Testing Framework
- Sitecore.FakeDb version 1.3.5

## Setup Prerequisites
The following applications/tools/utilities are required on your local machine. The appropriate version of these items should be installed based on the version of your operating system and appropriate licensing.
+ Sitecore XP 8.2 Update 2
+ IIS 8.5+
+ Visual Studio 2015 Update 3 or later
+ SQL Server 2012+

## Step-by-step Guide
1. Use [SIM](https://marketplace.sitecore.net/en/Modules/Sitecore_Instance_Manager.aspx) to setup a new **Sitecore 8.2 Update 2** instance whose site name should be `viethoang.local`
2. Site bindings: in case you want to create a new Sitecore instance manually, please remmember to bind one more site name `viethoang.local` to your Sitecore site
3. Open project folder, duplicate these files:
 * **gulp-config.js.example**: rename to **gulp-config.js**
 * **publishsettings.targets.example**: rename to **publishsettings.targets**
 * **\source\src\Foundation\VietHoang.Serialization\App_Config\Include\z.VietHoang.SerializationSettings.config.example**: rename to **z.VietHoang.SerializationSettings.config**
4. Run Visual Studio as **Administrator**, open the solution and update these files in `Configuration` folder
 + Open **gulp-config.js**: update `instanceRoot` to your Sitecore root folder
 + Open **publishsettings.targets**: update `<publishUrl>` to your sitecore website url if you don't have site name `viethoang.local`
 + Open **z.VietHoang.SerializationSettings.config**: update `sourceFolder` to your project folder
5. Install Node.js and restore all Node.js modules:
 * Download and install Node.js to your machine https://nodejs.org/en/download/
 * Run command line as **Administrator**.
 * Change directory to your project folder
 * Execute `npm install`
6. Run gulp tasks:
 * Open Task Runner Explorer on **View** -> **Other Window** -> **Task Runner Explorer**.
 * Choose `Solution 'VietHoang.Sitecore'` from list, then click refresh button.
 * From task list, double click on ``default`` task to run it.
7. Sitecore items serialization:
 + Open a browser, navigate to `viethoang.local/unicorn.aspx`, sign in with `admin` account then click `Sync` button in order to sync all Sitecore items.
 
## Test Your Local Environment
 + Open a browser, navigate to `viethoang.local/` and you should see the homepage without any errors
