var gulp = require("gulp");
var gulpif = require("gulp-if");
var msbuild = require("gulp-msbuild");
var debug = require("gulp-debug");
var foreach = require("gulp-foreach");
var rename = require("gulp-rename");
var watch = require("gulp-watch");
var newer = require("gulp-newer");
var runSequence = require("run-sequence");
var path = require("path");
var config = require("./gulp-config.js")();
var unicorn = require("./scripts/unicorn.js");
var fs = require('fs');
var viethoang = require("./scripts/VietHoang.js");

module.exports.config = config;

gulp.task("default", function (callback) {
    config.runCleanBuilds = true;
    return runSequence(
      "01-Sync-Unicorn",
    callback);
});


/*****************************
  Sync Unicorn
*****************************/
gulp.task("01-Sync-Unicorn", function (callback) {
    var options = {};
    options.siteHostName = viethoang.getSiteUrl();
    options.authenticationConfigFile = config.websiteRoot + "/App_config/Include/Unicorn/Unicorn.UI.config";

    unicorn(function () { return callback() }, options);
});