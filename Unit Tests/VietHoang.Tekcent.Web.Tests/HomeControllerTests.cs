﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VietHoang.Tekcent.Services.Interfaces;
using VietHoang.Tekcent.Web.Controllers;
using VietHoang.Tekcent.Web.Models;
using Sitecore.Data;
using Sitecore.FakeDb;
using VietHoang.Tekcent.Web.Tests.Stubs;
using SC = Sitecore;

namespace VietHoang.Tekcent.Web.Tests
{
    [TestClass]
    public class HomeControllerTests : BaseControllerTests
    {
        [TestMethod]
        public void CanInitializeHomeController()
        {
            var applicationServiceMock = new Mock<IApplicationService>();
            var controller = new HomeController(MapModelService, applicationServiceMock.Object);

            Assert.IsNotNull(controller);
        }

        [TestMethod]
        public void Index_Get_ReturnHomePageViewModel()
        {
            using (var scDb = new Db())
            {
                // Arrange
                var applicationService = new StubApplicationService();
                var homepage = applicationService.GetHomePage();

                var rootItem = new DbItem("Tekcent", ID.NewID);
                var homeItem = new DbItem("Home", new ID(homepage.Id));
                rootItem.Children.Add(homeItem);
                scDb.Add(rootItem);

                var controller = new HomeController(MapModelService, applicationService)
                {
                    ControllerContext = ControllerContext
                };

                var model = controller.MapModelService.Map<PageViewModel>(homepage);

                var siteContextFake = new SC.FakeDb.Sites.FakeSiteContext(
                       new SC.Collections.StringDictionary
                       {
                        {"name", "tekcent"},
                        {"database", "web"}
                       });

                using (new SC.Sites.SiteContextSwitcher(siteContextFake))
                {
                    // Act
                    var result = controller.Index() as ViewResult;

                    // Assert
                    Assert.IsNotNull(result);
                    Assert.IsInstanceOfType(result.Model, typeof(PageViewModel));
                    Assert.AreEqual(model.Id, ((PageViewModel)result.Model).Id);
                }
            }
        }
    }
}
