﻿using System;
using VietHoang.Tekcent.Domain.Models.Page;
using VietHoang.Tekcent.Services.Interfaces;

namespace VietHoang.Tekcent.Web.Tests.Stubs
{
    public class StubApplicationService : IApplicationService
    {
        public IPage GetHomePage()
        {
            return new Page { Id = new Guid("55db1250-bbca-44fc-be7a-c85f5ca21ec1") };
        }
    }
}
