﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using VietHoang.Shared.Extensions;

namespace VietHoang.Shared.Tests
{
    [TestClass]
    public class StringExtensionsTests
    {
        #region HasText Tests
        [TestMethod]
        public void HasText_StringIsNull_ReturnFalse()
        {
            // Arrange
            const string source = null;
            const bool expected = false;

            // Act
            var actual = source.HasText();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HasText_StringIsEmpty_ReturnFalse()
        {
            // Arrange
            const string source = "";
            const bool expected = false;

            // Act
            var actual = source.HasText();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HasText_StringHasWhitespace_ReturnFalse()
        {
            // Arrange
            const string source = "   ";
            const bool expected = false;

            // Act
            var actual = source.HasText();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HasText_StringHasText_ReturnTrue()
        {
            // Arrange
            const string source = " some text ";
            const bool expected = true;

            // Act
            var actual = source.HasText();

            // Assert
            Assert.AreEqual(expected, actual);
        }
        #endregion // HasText Tests

        #region GetGuidWithoutBraces tests

        [TestMethod]
        public void GetIdWithoutBraces_ItemNull_ReturnEmpty()
        {
            // Arrange
            const string source = null;
            var expected = string.Empty;

            // Act
            var actual = source.GetGuidWithoutBraces();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetIdWithoutBraces_ItemNotNull_ReturnValid()
        {
            // Arrange
            const string source = "{5CB56B6B-9646-4325-983C-D167E4E3CC61}";
            const string expected = "5CB56B6B-9646-4325-983C-D167E4E3CC61";
            // Act
            var actual = source.GetGuidWithoutBraces();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        #endregion

        #region ToInt Tests

        [TestMethod]
        public void ToInt_PassNullAndNull_ReturnNull()
        {
            // Arrange
            const string thisString = null;
            int? defaultValue = null;
            int? expected = null;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassNullAndZero_ReturnZero()
        {
            // Arrange
            const string thisString = null;
            const int defaultValue = 0;
            const int expected = 0;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassNullAndZeroNullable_ReturnZero()
        {
            // Arrange
            const string thisString = null;
            int? defaultValue = 0;
            int? expected = 0;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassEmptyAndNull_ReturnNull()
        {
            // Arrange
            const string thisString = "";
            int? defaultValue = null;
            int? expected = null;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void ToInt_PassEmptyAndZero_ReturnZero()
        {
            // Arrange
            const string thisString = "";
            const int defaultValue = 0;
            const int expected = 0;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassEmptyAndZeroNullable_ReturnZero()
        {
            // Arrange
            const string thisString = "";
            int? defaultValue = 0;
            int? expected = 0;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassInvalidAndNumber_ReturnNumber()
        {
            // Arrange
            const string thisString = "foo";
            const int defaultValue = 5;
            const int expected = 5;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassInvalidAndNumberNullable_ReturnNumber()
        {
            // Arrange
            const string thisString = "bar";
            int? defaultValue = 17;
            int? expected = 17;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassValidAndNumber_ReturnValid()
        {
            // Arrange
            const string thisString = "205";
            const int defaultValue = 5;
            const int expected = 205;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ToInt_PassValidAndNumberNullable_ReturnValid()
        {
            // Arrange
            const string thisString = "-17";
            int? defaultValue = null;
            int? expected = -17;

            // Act
            var actual = thisString.ToInt(defaultValue);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        #endregion // ToInt Tests

        #region IsNumber Tests
        [TestMethod]
        public void IsNumber_StringIsNull_ReturnFalse()
        {
            // Arrange
            const string source = null;
            const bool expected = false;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsNumber_StringIsEmpty_ReturnFalse()
        {
            // Arrange
            const string source = "";
            const bool expected = false;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsNumber_StringHasOnlyWhitespace_ReturnFalse()
        {
            // Arrange
            const string source = "   ";
            const bool expected = false;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsNumber_StringHasTextAndNumber_ReturnFalse()
        {
            // Arrange
            const string source = "text123";
            const bool expected = false;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsNumber_StringHasOnlyText_ReturnFalse()
        {
            // Arrange
            const string source = "text";
            const bool expected = false;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsNumber_StringHasOnlyNumber_ReturnTrue()
        {
            // Arrange
            const string source = "123";
            const bool expected = true;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsNumber_StringHasNumberAndWhiteSpace_ReturnTrue()
        {
            // Arrange
            const string source = "   123   ";
            const bool expected = true;

            // Act
            var actual = source.IsNumber();

            // Assert
            Assert.AreEqual(expected, actual);
        }
        #endregion // IsNumber Tests
    }
}
