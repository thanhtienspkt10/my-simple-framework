﻿using Glass.Mapper.Sc;
using Glass.Mapper.Sc.IoC;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sitecore.Data;
using VietHoang.Tekcent.Domain.Models.Page;
using SC = Sitecore;

namespace VietHoang.Tekcent.Services.Tests
{
    [TestClass]
    public class ApplicationServiceTests
    {
        [TestMethod]
        public void GetHomePage_ReturnHomePage()
        {
            // Arrange
            var homepageIdMock = ID.NewID;
            const bool isLazy = false;
            const bool inferType = false;

            var siteContextFake = new SC.FakeDb.Sites.FakeSiteContext(
                new SC.Collections.StringDictionary
                {
                    {"name", "tekcent"},
                    {"database", "web"}
                });

            var homePage = new Page { Id = homepageIdMock.Guid };

            var scContextMock = new Mock<ISitecoreContext>();
            scContextMock.Setup(
                x =>
                    x.GetItem<IPage>($"/sitecore/content/{siteContextFake.Name}/home", isLazy,
                        inferType))
                .Returns(homePage);

            var scContextFactoryMock=new Mock<ISitecoreContextFactory>();
            scContextFactoryMock.Setup(x => x.GetSitecoreContext()).Returns(scContextMock.Object);

            var applicationPageService = new ApplicationService(scContextFactoryMock.Object);

            using (new SC.Sites.SiteContextSwitcher(siteContextFake))
            {
                // Act
                var result = applicationPageService.GetHomePage();

                // Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(homePage, result);
            }
        }
    }
}
