var gulp = require("gulp");
var gulpif = require("gulp-if");
var msbuild = require("gulp-msbuild");
var debug = require("gulp-debug");
var foreach = require("gulp-foreach");
var rename = require("gulp-rename");
var watch = require("gulp-watch");
var newer = require("gulp-newer");
var runSequence = require("run-sequence");
var path = require("path");
var config = require("./gulp-config.js")();
var nugetRestore = require('gulp-nuget-restore');
var fs = require('fs');
var utils = require('./scripts/viethoangUtils.js');

module.exports.config = config;

gulp.task("default", function (callback) {
  config.runCleanBuilds = true;
  return runSequence(
    "01-Nuget-Restore",
    "02-Build-All-Test-Projects",
  callback);
});

/*****************************
  Initial setup
*****************************/

gulp.task("01-Nuget-Restore", function (callback) {
  var solution = "./" + config.solutionName + ".sln";
  return gulp.src(solution).pipe(nugetRestore());
});

gulp.task("02-Build-All-Test-Projects", function (callback) {
  return runSequence(
    "Build-Test-Projects", callback);
});


/*****************************
  Build Test Projects
*****************************/
var publishProjects = function (location, dest) {
  dest = dest || config.websiteRoot;
  var targets = ["Build"];
  if (config.runCleanBuilds) {
      targets = ["Clean", "Build"];
  }
  return gulp.src([location + "/**/*.csproj"])
    .pipe(foreach(function (stream, file) {

        var projectHasChangedFile = false;
        var projectDir = path.dirname(file.path);
        console.log("Project Dir: ", projectDir);
        var projectFiles = utils.getAllFilesFromProjectFolder(projectDir);
        projectFiles.some(function (file, index, array) {
            var fileStat = fs.statSync(file);
            var fileModifiedDate = new Date(fileStat.mtime);
            var lastSuccessfulBuildDateTime = new Date(config.lastSuccessfulBuildDateTime);
            if (fileModifiedDate > lastSuccessfulBuildDateTime) {
                projectHasChangedFile = true;
                console.log("Changed file: ", file);
                return true;
            }
            return false;
        });

      return stream
        .pipe(gulpif(!projectHasChangedFile, debug({ title: "STOP Building Test project:" })))
            .pipe(gulpif(projectHasChangedFile, debug({ title: "*** START Build Test project:" })))
            .pipe(gulpif(projectHasChangedFile, msbuild({
              targets: targets,
              configuration: config.buildConfiguration,
              logCommand: true,
              verbosity: "minimal",
              maxcpucount: 0,
              toolsVersion: 14.0,
			  errorOnFail: config.errorOnFail,
              stdout: config.errorOnFail,
              properties: {
              }
            })));
    }));
};

gulp.task("Build-Test-Projects", function () {
    return publishProjects("./Unit Tests");
});